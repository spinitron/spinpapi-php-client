SPINPAPI CLIENT PACKAGE README
==============================

Files included:

example.php                    Example web page using SpinPapi.
htmldocs                       HTML documentation of SpinPapiClient.
INSTALL_OS-X                   HOWTO install memcached on Mac OS X.
README                         This file.
signing-demo.php               PHP code to illustrate the algorithm for
                               signing requests. Not intended for use.
SpinPapiClient.inc.php         Package containing the client class.
SpinPapiConf.inc.php           Config file for spquery and eample.php.
spquery                        Command line utility to query SpinPapi.


SPINPAPI SPECIFICATION
======================

The current version of the SpinPapi API Specification is located at:
http://spinitron.com/user-guide/pdf/SpinPapi-v2.pdf


CACHING
=======

Caching of SpinPapi results is MANDATORY. This can be achieved in various ways, 
one of which is to use one of the caches built into SpinPapiClient.inc.php.

DO NOT code web pages that discard all SpinPapi results after each request!

Results from all SpinPapi methods can be cached for at least a minute and some 
for several hours.

SpinPapiClient.inc.php implements a generic, application-neutral caching policy 
that is described below under CACHING POLICY.


AUTHENTICATION
==============

To use SpinPapi you must have authentication cregedtials issued by Spinitron.
Email eva@spinitron.com to request yours.


SpinPapiClient
==============

SpinPapiClient.inc.php contains the SpinPapiClient class. This can be used to 
access SpinPapi or as a tutorial for how to do so. It is fully commented and
has (auto) documentation in the htmldocs directory. example.php and spquery
both use it.


CONFIGURAITION
==============

example.php and spquery share the config file: SpinPapiConf.inc.php. Follow the
instructions in the comments to make it work for you.


spquery UTILITY
===============

spquery is a PHP script. You need a CLI PHP interpreter on your system. You
might need to change the first line to suit uour system and/or the line
that starts with require_once and imports SpinPapiConf.inc.php if the client
lib is not in the same directory as the config file.

Examples:

$ ./spquery getSong
$ ./spquery -v getRegularShowsInfo When now

Assuming spquery is in your current working directory and $ is your command 
prompt.


example.php
===========

The example.php script produces an HTML page using live Spinitron data gotten
using SpinPapiClient. Do not cut and paste code from there into your web site.
It is purely a tutorial and commented throughout. You should come up with your 
own design and implement error handling fallbacks appropriate for your web 
site.

It demonstrates instantiating the SpinPapiClient, initializing one of its 
caches, making queries, processing results and using them in an HTML page.

It must be configured: see CONFIGURATION above.


CACHING POLICY
==============

There are three cache lifetimes: long, medium and short. You can adjust these.
They are class constants in SpinPapiClient.inc.php.

Short is for objects that can expire when the next song is added and is used 
for things like getSong and getCurrentPlaylist.

Long is for schedule, show and playlist metadata that seldom changes, e.g.
getRegularShowsInfo Now=5

Medium is for objects a bit more likely to change than long caching objects.

Here are the policy rules:

    If a valid numeric SongID parameter is specified or method is 
    getPlaylistsInfo then expiration is medium.

    If a valid numeric PlaylistID parameter is specified then expiration is long.

    If method is getRegularShowsInfo and When is invalid, absent or a digit 0..7 
    then expiration is long.

    If method is getRegularShowsInfo and When is now then expiration is short.

    If method is getRegularShowsInfo and When is today then expiration is the
    sooner of long and the start of the next schedule day (in the station's 
    timezone).

    Otherwise expiration is short.

In the case of getSongs with valid numeric PlaylistID, the specified playlist
could be the current in which case caching should be short. But this library
cannot determine if that's the case. So the caller should adopt one or both of 
two policies:

    DON'T USE getSongs(PlaylistID=N) WHEN GETTING THE CURRENT PLAYLIST

    DO OVERRIDE DEFAULT CACHE EXPIRATION IF N IN getSongs(PlaylistID=N) 
    MIGHT BE THE CURRENT PLAYLIST

Thus:
    getSong                         short
    getSong             SongID      medium
    getSongs                        short
    getSongs            PlaylistID  long (override if PlaylistID == current PL)
    getCurrentPlaylist              short
    getPlaylistInfo                 short
    getPlaylistInfo     PlaylistID  long
    getPlaylistsInfo                medium
    getShowInfo                     short
    getShowInfo         ShowID      long
    getRegularShowsInfo             long
    getRegularShowsInfo When=0..7   long
    getRegularShowsInfo When=today  min of long or end of today's schedule
    getRegularShowsInfo When=now    short


INSTALLING MEMCACHED
====================

On FreeBSD:
Depending what you prefer do:
    portmaster databases/pecl-memcached
or
    cd /usr/ports/databases/pecl-memcached
    make install clean
or
    portupgrade -r databases/pecl-memcached

On Mac OS X:
See INSTALL_OS-X

On Linux:
http://code.google.com/p/memcached/wiki/NewInstallFromPackage

From source:
http://code.google.com/p/memcached/wiki/NewInstallFromSource

Installing PECL Memcached extension
===================================
First, you need the libmemcached library. See:
http://libmemcached.org/libMemcached.html

Then install the PECL extension:
http://www.php.net/manual/en/memcached.installation.php

Security
========
Before running the memcached daemon, read and make sure you understand the 
security issues of networking with memcached. Read:
http://code.google.com/p/memcached/wiki/NewConfiguringServer

In particular, disable networking altogether and use a socket instead if you 
don't need it. If you use networking, limit the addresses memcached will 
service with the -l option and block external access with a local packet 
filter and/or firewall.
