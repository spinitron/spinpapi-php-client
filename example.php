<?php
/**
 * Example use of SpinPapiClient.
 *
 * This script is **only** as a tutorial. It produces an HTML page using live 
 * Spinitron data that it gets from SpinPapi using SpinPapiClient.
 * 
 * This script uses the configurations in SpinPapiConf.inc.php.
 *
 * PHP version 5.3
 *
 * Copyright (c) 2011-2013 Spinitron, LLC.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * - Neither the name of Spinitron, LLC nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @category    SpinPapi
 * @uses        SpinPapiClient SpinPapiConf
 * @author      Tom Worster <tom@spinitron.com> 
 * @copyright   2011-2013 Spinitron, LLC
 * @license     BSD 3-Clause License. http://opensource.org/licenses/BSD-3-Clause
 * @version     2013-03-04
 * @link        https://bitbucket.org/Spinitron/spinpapi-php-client
 */


require_once dirname(__FILE__) . '/SpinPapiConf.inc.php';


// Instantiate our client object.
$sp = new SpinPapiClient($mySpUserID, $mySpSecret, $myStation, true, $papiVersion);

// Initialize the cache according to the config file.
if (isset($myFCache)) {
	$sp->fcInit($myFCache);
}
if (isset($myMemcache)) {
	$sp->mcInit($myMemcache);
}

// A handy function for fomatting small time intervals. Presumes the HTML page 
// is encoded as UTF-8.
function my_seconds($n, $d=4) {
	if ( $n == 0 ) {
		return $n;
	} else {
		if ($n > 1) {
			$m = $n;
			$s = ' s';
			$e = max($d - 1 - floor(log($m,10)), 0);
		} elseif ($n > 0.001) {
			$m = 1000*$n;
			$s = ' ms';
			$e = min(max($d - 1 - floor(log($m,10)), 0), 3);
		} else {
			return round(1000000*$n) . ' μs';
		}
		return number_format($m, $e) . $s;
	}
}

// Shortcut to html encode a string
function e($s) { 
  return htmlspecialchars($s);
}



// Measure how long it takes for us to get the data from SpinPapi
$t = microtime(1);
$nowSong = $sp->query(array('method' => 'getSong'));
$t = my_seconds(microtime(1) - $t);

// You should perfom error checking. This example simply discards the 'success' 
// and 'request' parts of the SpinPapi responses:
$nowSong = $nowSong['results'];

// Nasty hack to store the timing info
$nowSong['t'] = $t;



$t = microtime(1);
$recentSongs = $sp->query(array('method' => 'getSongs', 'Num' => 3));
$t = my_seconds(microtime(1) - $t);
$recentSongs = $recentSongs['results'];
$recentSongs[0]['t'] = $t;



// You don't need to getPlaylistInfo for the current show if you already have 
// the now playing song because its response contains current playlist info.
// So this example is daft because we have what we need twice already.
$t = microtime(1);
$curPl = $sp->query(array('method' => 'getPlaylistInfo'));
$t = my_seconds(microtime(1) - $t);
$curPl = $curPl['results'];
$curPl['t'] = $t;



// Getting and using schedule info is a bit more complex. In this example we 
// want the first three scheduled shows today starting this hour. 

// date('G') gets the current 24-hour hour without leading zero. Take care 
// of PHP's conversion of integer strings starting with a 0 as octal.
$startHour = (int) date('G');
$t = microtime(1);
$curShows    = $sp->query(array(
	'method'    => 'getRegularShowsInfo', 
	'When'      => 'today', 
	'StartHour' => $startHour,
));
$t = my_seconds(microtime(1) - $t);
$curShows = $curShows['results'];

// getRegularShowsInfo doesn't sort the results so we do.
function myShowSort($a, $b) {
	global $startHour;
	list($ah, $am) = explode(':', $a['OnairTime']);
	list($bh, $bm) = explode(':', $b['OnairTime']);
	$ah = ($ah < $startHour ? $ah + 24 : $ah) + $am/60;
	$bh = ($bh < $startHour ? $bh + 24 : $bh) + $bm/60;
	return $ah > $bh ? 1 : -1;
}
usort($curShows, 'myShowSort');
$curShows[0]['t'] = $t;



// That's enough data, now generate an HTML page.

?><!DOCTYPE html>
<!--[if lt IE 7]>  <html class="no-js ie ie6 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 7]>     <html class="no-js ie ie7 lte9 lte8 lte7"> <![endif]-->
<!--[if IE 8]>     <html class="no-js ie ie8 lte9 lte8"> <![endif]-->
<!--[if IE 9]>     <html class="no-js ie ie9 lte9"> <![endif]-->
<!--[if gt IE 9]>  <html class="no-js"> <![endif]-->
<!--[if !IE]><!--> <html class="no-js">  <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>SpinPapi Demo</title>
  <style type="text/css" media="all">
    html, body, div, span, object, iframe, h1, h2, h3, h4, h5, 
    h6, p, blockquote, pre, a, abbr, acronym, address, cite, code, 
    del, dfn, em, img, ins, kbd, q, samp, strong, sub, sup, tt, var, 
    dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, 
    caption, tbody, tfoot, thead, tr, th, td {
      margin:0;
      padding:0;
      outline:0;
      font-size:1em;
      }
    body {
      color: #000;
      background-color: #f6f2e2;
      font-family: Corbel, "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", 
        "DejaVu Sans", "Bitstream Vera Sans", "Liberation Sans", Arial, Verdana, 
        "Verdana Ref", sans-serif;
      font-size: 100%;
      padding: 0 2em 4em;
      }
    a {
      color: #a00;
      text-decoration: none;
      }
    a:hover { 
      color: #e00;
      }
    b {
      font-weight: bold;
      color: #393939;
      }
    h1 {
      font-size: 1.25em;
      margin: 1.1em 0 0.1em -1.6em;
      padding: 0.2em 0 0.2em 1.6em;
      text-transform: uppercase;
      max-width: 32em;
      background-color: #ddd;
      }
    html.lte7 h1 {
      width: 32em;
      }
    h2 {
      font-size: 1.16em;
      margin: 1.1em 0 0;
      max-width: 33.2em;
      }
    html.lte7 h2 {
      width: 33.2em;
      }
    p {
      font-size: 1em;
      margin: 0.5em 0 0;
      max-width: 40em;
      text-align: justify;
      }
    html.lte7 p {
      width: 40em;
      }
    ul, ol {
      padding: 0 0 0 2em;
      text-align: justify;
    }
    ul {
      list-style: disc outside none;
      }
    li {
      max-width: 38em;
      font-size: 1em;
      margin: 0.3em 0 0 0;
      }
    li:first-child {
      margin-top: 0.5em;
      }
    hr {
      max-width: 40em;
      margin: 2.5em 0 0;
      }
    html.lte7 hr {
      width: 40em;
      }
  </style>
</head>
<body>

  <h1>SpinPapi example</h1>

  <h2>Now playing (<?php echo $nowSong['t']; ?>)</h2>
  <p><?php
      // NOTE: None of these examples check the SpinPapi result for errors or
      // unusuable data. You should!
      echo 
        "<a href=\"http://spinitron.com/radio/playlist.php"
        . "?station=$myStation&amp;playlist={$nowSong['PlaylistID']}#{$nowSong['SongID']}\">"
        . e($nowSong['ArtistName']) . ' – ' . e($nowSong['SongName'])
        . '</a>';
    ?></p>

  <h2>Recent spins (<?php echo $recentSongs[0]['t']; ?>)</h2>
  <?php
    foreach ($recentSongs as $song) {
      echo '<p>' . e($song['ArtistName']) . ' – ' . e($song['SongName']) . '</p>';
    }
  ?>

  <h2>Current playlist (<?php echo $curPl['t']; ?>)</h2>
  <p><?php
      echo 
        "<a href=\"http://spinitron.com/radio/playlist.php"
        . "?station=$myStation&amp;playlist={$curPl['PlaylistID']}\">"
        . e($curPl['ShowName']) . ' with ' . e($curPl['DJName'])
        . '</a>';
    ?></p>

  <h2>Today on <?php 
      echo strtoupper(e($myStation)) 
	  . ' (' . $curShows[0]['t'] . ")</h2>\n"; 
    for ($i = 0; $i < 3 && isset($curShows[$i]); ++$i) {
      $show =& $curShows[$i];
      echo 
        '<p>' 
        . date('H:ia', strtotime($show['OnairTime']))
        . ' - ' . date('H:ia', strtotime($show['OffairTime']))
        . ": <a href=\"http://spinitron.com/radio/playlist.php"
        . "?station=$myStation&amp;showid={$show['ShowID']}\">"
        . e($show['ShowName']) 
        . '</a></p>';
    }
  ?>

</body>
</html>