set tabstop=4
USING SPINPAPI WITH MEMCACHED AND PHP
=====================================


Installing memcached
====================

On FreeBSD:
Depending what you prefer do:
	portmaster databases/pecl-memcached
or
	cd /usr/ports/databases/pecl-memcached
	make install clean
or
	portupgrade -r databases/pecl-memcached

On Mac OS X:
See INSTALL_OS-X

On Linux:
http://code.google.com/p/memcached/wiki/NewInstallFromPackage

From source:
http://code.google.com/p/memcached/wiki/NewInstallFromSource


Installing PECL Memcached extension
===================================

First, you need the libmemcached library. See:
http://libmemcached.org/libMemcached.html

Then install the PECL extension:
http://www.php.net/manual/en/memcached.installation.php


Security
========

Before running the memcached daemon, read and make sure you understand the 
security issues of networking with memcached. Read:
http://code.google.com/p/memcached/wiki/NewConfiguringServer

In particular, disable networking altogether and use a socket instead if you 
don't need it. If you use networking, limit the addresses memcached will 
service with the -l option and block external access with a local packet 
filter and/or firewall.


Using the caching SpinPapi PHP client
=====================================

Put spinpapi_memcached.inc.php in your include path. It is a drop-in replacement
for spinpapi.inc.php.

Configure your memcached servers in spinpapi_memcached.inc.php.

You can adjust the short, medium and long cache expiration times. To change the 
caching policy in any other way you will need to modify the code. Key generation
affects caching policy but you probably don't want to change that. The 
expiration time selections are explained below.

Note the new $cache_expiration override argument for spin_papi_query(). This
allows the client to explicitly set cache expiration time in seconds. 


Caching policy
==============

There are three cache lifetimes: long, medium and short.

Short is for objects that can expire when the next song is added and is used 
for things like getSong and getCurrentPlaylist.

Long is for schedule, show and playlist metadata that seldom changes, e.g.
getRegularShowsInfo	Now=5

Medium is for objects a bit more likely to change than long caching objects.

Here are the policy rules:

	If a valid numeric SongID parameter is specified or method is 
		getPlaylistsInfo then expiration is medium.

	If a valid numeric PlaylistID parameter is specified then expiration is long.

	If method is getRegularShowsInfo and When is invalid, absent or a digit 0..7 
		then expiration is long.

	If method is getRegularShowsInfo and When is now then expiration is short.

	If method is getRegularShowsInfo and When is today then expiration is the
	sooner of long and the start of the next schedule day (in the station's 
	timezone).

	Otherwise expiration is short.

In the case of getSongs with valid numeric PlaylistID, the specified playlist
could be the current in which case caching should be short. But this library
cannot determine if that's the case. So the caller should adopt one or both of 
two policies:

	DON'T USE getSongs(PlaylistID=N) WHEN GETTING THE CURRENT PLAYLIST

	OVERRIDE DEFAULT CACHE EXPIRATION IF N IN getSongs(PlaylistID=N) MIGHT BE
	THE CURRENT PLAYLIST

Thus:
	getSong							short
	getSong				SongID		medium
	getSongs						short
	getSongs			PlaylistID	long (override if PlaylistID == current PL)
	getCurrentPlaylist				short
	getPlaylistInfo					short
	getPlaylistInfo		PlaylistID	long
	getPlaylistsInfo				medium
	getShowInfo						short
	getShowInfo			ShowID		long
	getRegularShowsInfo				long
	getRegularShowsInfo	When=0..7	long
	getRegularShowsInfo	When=today	min of long or end of today's schedule
	getRegularShowsInfo	When=now	short

