<?php
/**
 * Demonstration of SpinPapi request signing.
 * 
 * This code is intended **only** for teaching the SpinPapi request signing process
 * in tandem with the SpinPapi documentation.
 *
 * PHP version 5.3
 *
 * Copyright (c) 2011-2013 Spinitron, LLC.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 * - Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 * - Neither the name of Spinitron, LLC nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 *
 * @category    SpinPapi
 * @author      Tom Worster <tom@spinitron.com>
 * @copyright   2011-2013 Spinitron, LLC
 * @license     BSD 3-Clause License. http://opensource.org/licenses/BSD-3-Clause
 * @version     2013-03-04
 * @link        https://bitbucket.org/Spinitron/spinpapi-php-client
 */

$params = array(
    'method' => 'getSong',
    'station' => 'wxyz',
    'papiversion' => '1',
    'papiuser' => '77c6d6c12a8d9173', // use your userid
    'timestamp' => gmdate('Y-m-d\TH:i:s\Z'),
    'SongID' => 12345,
);

// Sort the list of pairs alphabetically by parameter name.
ksort($params);

// Collect a list of URL-encoded name=value strings in $query.
$query = array();
foreach ($params as $name => $value) {
    // URL-encode each parameter names and each parameter value.
    // Combine each pair into a string using an = character as separator.
    $query[] = rawurlencode($name) . '=' . rawurlencode($value);
}

// Combine all the name=value strings using & characters as separators.
$query = implode('&', $query);

// Form a signing subject by combining the host name, relative URL and
// query string using newlines as separators.
$subject = "spinitron.com\n/public/spinpapi.php\n" . $query;

// Calculate the HMAC of the signing subject using your shared secret.
$signature = hash_hmac("sha256", $subject, '5e99a8be74cb4a4c', true);

// base64-encode the signature to produce the signature parameter value.
$signature = base64_encode($signature);

// Form a name=value string as for the other parameters above.
$signature = rawurlencode('signature') . '=' . rawurlencode($signature);

// Append the name=value string to the query using & as separator.
$query .= '&' . $signature;

// Form the full request URL.
$request = 'http://spinitron.com/public/spinpapi.php?' . $query;

// Send the request and collect the JSON-encoded response body.
$response = file_get_contents($request);

// Display the JSON-decoded response.
var_export(json_decode($response, 1));
